<?php


namespace Magenest\Attachment\Ui\Component\Listing\Column\Attachment;

use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class FileIcon
 * @package Magenest\Attachment\Ui\Component\Listing\Column\Attachment
 */
class FileIcon extends Column
{
    /**
     * @var \Magenest\Attachment\Helper\AttachmentHelper
     */
    protected $attachmentHelper;

    /**
     * FileIcon constructor.
     * @param \Magenest\Attachment\Helper\AttachmentHelper $attachmentHelper
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        \Magenest\Attachment\Helper\AttachmentHelper $attachmentHelper,
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->attachmentHelper = $attachmentHelper;
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $iconUrl = $this->attachmentHelper->getIconExtension($item['file_extension'] ?? '');
                $item['file_icon'] = '<img style="max-height: 40px;" class="attachment-file-icon" src="'.$iconUrl.'"/>';
            }
        }
        return $dataSource;
    }
}