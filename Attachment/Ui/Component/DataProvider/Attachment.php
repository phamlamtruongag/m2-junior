<?php


namespace Magenest\Attachment\Ui\Component\DataProvider;


use Magento\Ui\DataProvider\AbstractDataProvider;

/**
 * Class Attachment
 * @package Magenest\Attachment\Ui\Component\DataProvider
 */
class Attachment extends AbstractDataProvider
{
    /**
     * @var
     */
    private   $loadedData;
    /**
     * @var \Magenest\Attachment\Model\ResourceModel\Attachment\CollectionFactory
     */
    protected $attachmentCollection;

    /**
     * Attachment constructor.
     * @param \Magenest\Attachment\Model\ResourceModel\Attachment\CollectionFactory $attachmentCollection
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        \Magenest\Attachment\Model\ResourceModel\Attachment\CollectionFactory $attachmentCollection,
        $name,
        $primaryFieldName,
        $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->attachmentCollection = $attachmentCollection;
        $this->collection = $this->attachmentCollection->create();
    }

    /**
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        foreach ($this->collection->getItems() as $item) {
            $data = $item->getData();
            $data['file_upload'][0] = json_decode($data['file_detail'],true);
            $data['container_group']['file_name'] = $data['file_name'];
            $data['container_group']['file_extension'] = $data['file_extension'];
            $data['customer_group_ids'] = json_decode($data['customer_group_ids'],true);
            $this->loadedData[$item->getId()] = $data;
        }
        return $this->loadedData;
    }
}