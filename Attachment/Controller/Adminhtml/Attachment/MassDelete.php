<?php


namespace Magenest\Attachment\Controller\Adminhtml\Attachment;


class MassDelete extends \Magento\Backend\App\Action
{
    protected $attachmentCollection;
    protected $logger;
    protected $filter;
    protected $cache;

    public function __construct(
        \Magenest\Attachment\Model\ResourceModel\Attachment\CollectionFactory $attachmentCollection,
        \Magento\Framework\App\Cache\TypeListInterface $cache,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Backend\App\Action\Context $context
    ) {
        parent::__construct($context);
        $this->attachmentCollection = $attachmentCollection;
        $this->logger = $logger;
        $this->filter = $filter;
        $this->cache = $cache;
    }

    public function execute()
    {
        try{
            $collection = $this->filter->getCollection($this->attachmentCollection->create());
            $count = 0;
            foreach ($collection->getItems() as $item){
                $item->delete();
                $count++;
            }
            $this->cache->invalidate('full_page');
            $this->messageManager->addSuccess(
                __('A total of %1 record(s) have been deleted.', $count)
            );
        }catch (\Exception $exception){
            $this->messageManager->addError($exception->getMessage());
            $this->logger->critical($exception->getMessage());
        }
        return $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT)->setPath('*/*/index');
    }
}