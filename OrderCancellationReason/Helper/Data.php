<?php


namespace Magenest\OrderCancellationReason\Helper;


class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_REASON_CANCELLATION = 'sales/order_cancellation_reason/reason';

    protected $scopeConfig;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
        $this->scopeConfig = $scopeConfig;
    }

    public function getReasonCancel() {
        $reason = json_decode($this->scopeConfig->getValue(self::XML_PATH_REASON_CANCELLATION,\Magento\Store\Model\ScopeInterface::SCOPE_STORE), true);
        return $reason;
    }
}