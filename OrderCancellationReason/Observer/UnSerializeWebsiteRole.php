<?php


namespace Magenest\OrderCancellationReason\Observer;


class UnSerializeWebsiteRole implements \Magento\Framework\Event\ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $user = $observer->getObject();
        $websiteRole = $user->getWebsiteRole();
        if (is_string($websiteRole)) {
            $user->setWebsiteRole(json_decode($websiteRole ?? '',true));
        }
    }
}