<?php


namespace Magenest\OrderCancellationReason\Observer;


class SerializeWebsiteRole implements \Magento\Framework\Event\ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $user = $observer->getObject();
        $websiteRole = $user->getWebsiteRole() ?? [];
        if (is_array($websiteRole)) {
            $user->setWebsiteRole(json_encode($websiteRole));
        }
    }
}