<?php


namespace Magenest\OrderCancellationReason\Api;


/**
 * Interface CustomerOrderRepositoryInterface
 * @package Magenest\OrderCancellationReason\Api
 */
interface CustomerOrderRepositoryInterface
{
    /**
     * @return \Magento\Sales\Api\Data\OrderInterface[]
     */
    public function getList();

    /**
     * @param int $id
     * @return \Magento\Sales\Api\Data\OrderInterface | bool
     */
    public function get($id);

    /**
     * @param int $id
     * @param string $reason
     * @param string $comment
     * @return \Magento\Framework\Phrase | string | bool
     */
    public function cancelOrder($id, $reason, $comment);
}