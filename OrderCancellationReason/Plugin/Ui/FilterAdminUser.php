<?php


namespace Magenest\OrderCancellationReason\Plugin\Ui;


class FilterAdminUser
{
    protected $authSession;

    public function __construct(
        \Magento\Backend\Model\Auth\Session $authSession
    ) {
        $this->authSession = $authSession;
    }

    public function afterGetReport(\Magento\Framework\View\Element\UiComponent\DataProvider\CollectionFactory $subject, $collection,$requestName)
    {
        if ($requestName == 'magenest_cancellation_request_listing_data_source') {
            $user = $this->authSession->getUser();
            $websiteRole = $user->getWebsiteRole();
            if ($websiteRole) {
                $collection->addFieldToFilter('website_id',['in' => $websiteRole]);
            }
        }
        return $collection;
    }
}