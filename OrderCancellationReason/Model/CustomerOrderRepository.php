<?php


namespace Magenest\OrderCancellationReason\Model;


class CustomerOrderRepository implements  \Magenest\OrderCancellationReason\Api\CustomerOrderRepositoryInterface
{
    protected $orderCollectionFactory;

    protected $orderConfig;

    protected $compositeUserContext;

    protected $cancellationReasonFactory;

    protected $cancellationReasonResource;

    protected $orderHistoryFactory;

    protected $orderRepository;

    public function __construct(
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Model\Order\Status\HistoryFactory $orderHistoryFactory,
        \Magenest\OrderCancellationReason\Model\ResourceModel\CancellationReason $cancellationReasonResource,
        \Magenest\OrderCancellationReason\Model\CancellationReasonFactory $cancellationReasonFactory,
        \Magento\Authorization\Model\CompositeUserContext $compositeUserContext,
        \Magento\Sales\Model\Order\Config $orderConfig,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
    ) {
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->orderConfig = $orderConfig;
        $this->compositeUserContext = $compositeUserContext;
        $this->cancellationReasonFactory = $cancellationReasonFactory;
        $this->cancellationReasonResource = $cancellationReasonResource;
        $this->orderHistoryFactory = $orderHistoryFactory;
        $this->orderRepository = $orderRepository;
    }

    public function getList()
    {
        $orders = $this->orderCollectionFactory->create($this->getCustomerId())->addFieldToSelect(
            '*'
        )->addFieldToFilter(
            'status',
            ['in' => $this->orderConfig->getVisibleOnFrontStatuses()]
        )->setOrder(
            'created_at',
            'desc'
        );
        return $orders->getItems();
    }

    public function get($id)
    {
        $orders = $this->orderCollectionFactory->create($this->getCustomerId())->addFieldToSelect(
            '*'
        )->addFieldToFilter(
            'entity_id',
            ['eq' => $id]
        )->addFieldToFilter(
            'status',
            ['in' => $this->orderConfig->getVisibleOnFrontStatuses()]
        )->setOrder(
            'created_at',
            'desc'
        );
        return count($orders) ? $orders->getFirstItem() : false;
    }

    public function getCustomerId() {
        return $this->compositeUserContext->getUserId();
    }

    public function cancelOrder(
        $id,
        $reason,
        $comment
    ) {
        $order = $this->orderCollectionFactory->create($this->getCustomerId())->addFieldToSelect(
            '*'
        )->addFieldToFilter(
            'entity_id',
            ['eq' => $id]
        )->addFieldToFilter(
            'status',
            ['in' => $this->orderConfig->getVisibleOnFrontStatuses()]
        )->setOrder(
            'created_at',
            'desc'
        )->getFirstItem();
        $cancellationReason = $this->cancellationReasonFactory->create();
        if ($order->getId()) {
            $this->cancellationReasonResource->load($cancellationReason,$order->getId(),'order_id');
            $cancellationReason->setOrderId($order->getIncrementId());
            $cancellationReason->setComment($comment);
            $cancellationReason->setReason($reason);
            $cancellationReason->setStoreId($order->getStoreId());
            $cancellationReason->setCustomerEmail($order->getCustomerEmail());
            $this->cancellationReasonResource->save($cancellationReason);
            if ($order->canComment()) {
                $history = $this->orderHistoryFactory->create()
                                                     ->setStatus($order->getStatus())
                                                     ->setEntityName(\Magento\Sales\Model\Order::ENTITY)
                                                     ->setComment(__('Cancellation reason: %1. Comment: %2', $reason, $comment));

                $history->setIsCustomerNotified(true)
                        ->setIsVisibleOnFront(true);
                $order->addStatusHistory($history);
                $this->orderRepository->save($order);
            }
            return true;
        }
        return __('Could not found order.');
    }
}