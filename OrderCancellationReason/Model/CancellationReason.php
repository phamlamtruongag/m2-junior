<?php


namespace Magenest\OrderCancellationReason\Model;


class CancellationReason extends \Magento\Framework\Model\AbstractModel
{
    public function _construct()
    {
        $this->_init(ResourceModel\CancellationReason::class);
    }
}