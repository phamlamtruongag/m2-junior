<?php


namespace Magenest\OrderCancellationReason\Model\ResourceModel\CancellationReason;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';

    public function _construct()
    {
        $this->_init(\Magenest\OrderCancellationReason\Model\CancellationReason::class,\Magenest\OrderCancellationReason\Model\ResourceModel\CancellationReason::class);
    }
}