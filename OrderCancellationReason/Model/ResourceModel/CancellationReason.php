<?php


namespace Magenest\OrderCancellationReason\Model\ResourceModel;


class CancellationReason extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init('magenest_order_cancellation_reason','id');
    }
}