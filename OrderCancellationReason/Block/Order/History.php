<?php


namespace Magenest\OrderCancellationReason\Block\Order;

use Magento\Framework\App\ObjectManager;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactoryInterface;

class History extends \Magento\Sales\Block\Order\History
{
    private $orderCollectionFactory;

    protected $orderRepository;

    protected $orderCancellationHelper;

    protected $cancellationReasonFactory;

    protected $cancellationReasonResource;

    protected $cancellationReasonCollectionFactory;

    public function __construct(
        \Magenest\OrderCancellationReason\Model\ResourceModel\CancellationReason\CollectionFactory $cancellationReasonCollectionFactory,
        \Magenest\OrderCancellationReason\Model\ResourceModel\CancellationReason $cancellationReasonResource,
        \Magenest\OrderCancellationReason\Model\CancellationReasonFactory $cancellationReasonFactory,
        \Magenest\OrderCancellationReason\Helper\Data $orderCancellationHelper,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Sales\Model\Order\Config $orderConfig,
        array $data = []
    ) {
        parent::__construct($context, $orderCollectionFactory, $customerSession, $orderConfig, $data);
        $this->orderRepository = $orderRepository;
        $this->orderCancellationHelper = $orderCancellationHelper;
        $this->cancellationReasonFactory = $cancellationReasonFactory;
        $this->cancellationReasonResource = $cancellationReasonResource;
        $this->cancellationReasonCollectionFactory = $cancellationReasonCollectionFactory;
    }

    public function canCancel($orderId) {
        $cancellationReasonCollection = $this->cancellationReasonCollectionFactory->create()
                                                                                  ->addFieldToFilter('order_id',$orderId);
        return $this->orderRepository->get($orderId)->canCancel() && count($cancellationReasonCollection) == 0;
    }

    private function getOrderCollectionFactory()
    {
        if ($this->orderCollectionFactory === null) {
            $this->orderCollectionFactory = $this->orderCollectionFactory->create();
        }
        return $this->orderCollectionFactory;
    }

    public function getReasonCancel() {
        return $this->orderCancellationHelper->getReasonCancel();
    }
}