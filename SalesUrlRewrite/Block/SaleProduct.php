<?php


namespace Magenest\SalesUrlRewrite\Block;


use Magento\Framework\View\Element\Template;

class SaleProduct extends Template
{
    protected $collection;
    protected $imageBuilder;
    protected $saleHelper;

    public function __construct(
        \Magenest\SalesUrlRewrite\Helper\SaleHelper $saleHelper,
        \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder,
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->imageBuilder = $imageBuilder;
        $this->saleHelper = $saleHelper;
        $this->collection = $this->saleHelper->initCollection();
    }

    public function getProductCollection() {
        return $this->collection;
    }

}