<?php


namespace Magenest\SalesUrlRewrite\Helper;


class SaleHelper extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $urlRewriteCollection;
    protected $collection;
    protected $productCollection;

    public function __construct(
        \Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollectionFactory $urlRewriteCollection,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollection,
        \Magento\Framework\App\Helper\Context $context)
    {
        parent::__construct($context);
        $this->urlRewriteCollection = $urlRewriteCollection;
        $this->productCollection = $productCollection;
    }

    public function initCollection()
    {
        if (!$this->collection) {
            $urlRewriteCollection = $this->urlRewriteCollection->create()->addFieldToFilter('request_path',['like'=>'sale/%']);
            $productIds = [];
            foreach ($urlRewriteCollection as $item) {
                $targetPath = explode('/',$item->getTargetPath());
                $productIds[] = end($targetPath);
            }
            $this->collection = $this->productCollection->create()->addFieldToFilter('entity_id',['in'=>$productIds])
                                                        ->addAttributeToSelect('name')
                                                        ->addAttributeToSelect('small_image');
        }
        return $this->collection;
    }


}