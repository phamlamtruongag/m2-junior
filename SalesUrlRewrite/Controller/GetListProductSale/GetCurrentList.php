<?php


namespace Magenest\SalesUrlRewrite\Controller\GetListProductSale;


class GetCurrentList extends \Magento\Framework\App\Action\Action
{
    protected $jsonResult;
    protected $imageBuilder;
    protected $collection;
    protected $saleHelper;

    public function __construct(
        \Magenest\SalesUrlRewrite\Helper\SaleHelper $saleHelper,
        \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder,
        \Magento\Framework\Controller\Result\JsonFactory $jsonResult,
        \Magento\Framework\App\Action\Context $context)
    {
        parent::__construct($context);
        $this->jsonResult = $jsonResult;
        $this->imageBuilder = $imageBuilder;
        $this->saleHelper = $saleHelper;
        $this->collection = $this->saleHelper->initCollection();
    }

    protected function getProductCollection($curPage = 1) {
        return $this->collection->setCurPage($curPage)->setPageSize(8);
    }

    protected function getImage($product, $imageId, $attributes = [])
    {
        return $this->imageBuilder->setProduct($product)
                                  ->setImageId($imageId)
                                  ->setAttributes($attributes)
                                  ->create();
    }

    public function execute()
    {
        $result = $this->jsonResult->create();
        $data = '<table class="product-sale-table">
            <tr>';
        $productCollection = $this->getProductCollection($this->getRequest()->getParam('cur_page'));
        $count = 0;
        foreach ($productCollection as $product){
            $count++;
            $productImage = $this->getImage($product, 'product_small_image');
            $data .= ' <td class="col product">
                    <p>
                        <a href="'.$product->getProductUrl().'">
                            <img class="sale-product-image" src="'.$productImage->getImageUrl().'" />
                        </a>
                    </p>
                    <p>
                        <a href="'.$product->getProductUrl().'">
                            <strong>'.$product->getName().'</strong>
                        </a>
                    </p>
                    <p>
                        <a href="'.$product->getProductUrl().'">
                            View Product
                        </a>
                    </p>
                </td>';
            if ($count % 4 == 0) {
                $data .= '</tr>';
                if ($count < 8) {
                    $data .= '<tr>';
                }
            }
            }
        $result->setData($data);
        return $result;
    }
}