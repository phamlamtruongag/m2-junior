<?php


namespace Magenest\SalesUrlRewrite\Observer;


class AddUrlRewriteSaleProduct implements \Magento\Framework\Event\ObserverInterface
{
    protected $logger;
    protected $storeManager;
    protected $urlRewriteResource;
    protected $urlRewriteModel;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\UrlRewrite\Model\ResourceModel\UrlRewrite $urlRewriteResource,
        \Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewriteModel)
    {
        $this->logger = $logger;
        $this->storeManager = $storeManager;
        $this->urlRewriteResource = $urlRewriteResource;
        $this->urlRewriteModel = $urlRewriteModel;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $observer->getEvent()->getProduct();
        if ($product->getSpecialPrice() && $product->getSpecialPrice() < $product->getPrice()) {
            try {
                $urlRewriteModel = $this->urlRewriteModel->create();
                $requestPath = 'sale/'.urlencode($product->getSku());
                $targetPath = 'catalog/product/view/id/'.$product->getId();
                $this->urlRewriteResource->load($urlRewriteModel,$requestPath,'request_path');
                $urlRewriteModel->setRequestPath($requestPath);
                $urlRewriteModel->setTargetPath($targetPath);
                $urlRewriteModel->setStoreId($this->storeManager->getDefaultStoreView()->getId());
                $this->urlRewriteResource->save($urlRewriteModel);
            } catch (\Exception $e) {
                $this->logger->critical($e);
            }
        }
    }
}