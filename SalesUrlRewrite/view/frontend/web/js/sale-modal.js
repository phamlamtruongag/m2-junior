define([
    'jquery',
    'Magento_Ui/js/modal/modal',
    'mage/url',
    'mage/translate'
], function ($, modal, urlBuilder) {
    'use strict';
    return function (config) {
        var curPage = 1;
        var options = {
            type: 'popup',
            responsive: true,
            innerScroll: true,
            title: 'List Product Sale',
            buttons: [
                {
                    text: $.mage.__('Back'),
                    class: 'sale-product-back-button',
                    click: function () {
                        if (curPage > 1) {
                            curPage--;
                            if (config.countProduct > 8) {
                                $(".sale-product-next-button").show();
                            }
                        } else {
                            $(".sale-product-back-button").hide();
                        }
                        $.ajax({
                            type: 'GET',
                            data: {
                                cur_page: curPage,
                            },
                            url: urlBuilder.build('sale_urlrewrite/getlistproductsale/getcurrentlist'),
                            showLoader: true,
                            success: function (response) {
                                $("#modal-content").html(response);
                            },
                            error: function () {
                            }
                        });
                    }
                },
                {
                    text: $.mage.__('Next'),
                    class: 'sale-product-next-button',
                    click: function () {
                        curPage ++;
                        if (curPage > 1) {
                            $(".sale-product-back-button").show();
                        }
                        if (curPage*8 >= config.countProduct) {
                            $(".sale-product-next-button").hide();
                        }
                        $.ajax({
                            type: 'GET',
                            data: {
                                cur_page: curPage,
                            },
                            url: urlBuilder.build('sale_urlrewrite/getlistproductsale/getcurrentlist'),
                            showLoader: true,
                            success: function (response) {
                                $("#modal-content").html(response);
                            },
                            error: function () {
                            }
                        });
                    }
                },
                {
                    text: $.mage.__('Close'),
                    class: 'close-button',
                    click: function () {
                        this.closeModal();
                    }
                },
            ]
        };

        var popup = modal(options, $('#sale-modal'));
        $(".sale-product-back-button").hide();
        if (config.countProduct <= 8) {
            $(".sale-product-next-button").hide();
        }
        $("#sale-button").on('click',function(){
            curPage = 1;
            $.ajax({
                type: 'GET',
                data: {
                    cur_page: curPage,
                },
                url: urlBuilder.build('sale_urlrewrite/getlistproductsale/getcurrentlist'),
                showLoader: true,
                success: function (response) {
                    $("#modal-content").html(response);
                },
                error: function () {
                }
            });
            $("#sale-modal").modal("openModal");
        });
    }
});
