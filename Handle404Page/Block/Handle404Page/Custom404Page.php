<?php


namespace Magenest\Handle404Page\Block\Handle404Page;


use Magento\Framework\View\Element\Template;

class Custom404Page extends Template
{
    protected $categoryRepository;

    public function __construct(
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->categoryRepository = $categoryRepository;
    }

    public function getCategorySuggest() {
        return $this->_request->getParam('categorySuggest') ? $this->categoryRepository->get($this->_request->getParam('categorySuggest')) : null;
    }

}