<?php


namespace Magenest\Handle404Page\Plugin;


class CustomNoRoute
{
    protected $layerResolver;
    protected $queryCollection;
    protected $redirectResult;
    protected $categoryCollection;
    protected $request;

    public function __construct(
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        \Magento\Search\Model\ResourceModel\Query\CollectionFactory $queryCollection,
        \Magento\Backend\Model\View\Result\RedirectFactory $redirectResult,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollection,
        \Magento\Framework\App\RequestInterface $request)
    {
        $this->layerResolver = $layerResolver;
        $this->queryCollection = $queryCollection;
        $this->redirectResult = $redirectResult;
        $this->categoryCollection = $categoryCollection;
        $this->request = $request;
    }

    public function afterExecute(\Magento\Cms\Controller\Noroute\Index $subject, $result)
    {
        $requestValue = ltrim($this->request->getPathInfo(), '/');
        $requestValue = str_replace('-',' ',$requestValue);
        $requestKeyword = explode('/',$requestValue);
        $this->layerResolver->create(\Magento\Catalog\Model\Layer\Resolver::CATALOG_LAYER_SEARCH);
        foreach ($requestKeyword as $keyword) {
            if ($keyword) {
                $categoryCollection = $this->categoryCollection->create()->addFieldToFilter('name',['eq'=>$keyword]);
                if ($categoryCollection->count()) {
                    $categorySuggest = $categoryCollection->getFirstItem();
                    $this->request->setParam('categorySuggest',$categorySuggest->getId());
                    break;
                }
                $queryCollection = $this->queryCollection->create()->addFieldToFilter('query_text',['eq'=>$keyword]);
                if ($queryCollection->count()) {
                    $querySearch = $queryCollection->getFirstItem();
                    return $this->redirectSearchPage($querySearch->getQueryText());
                }
                if (strlen($keyword) >= 3) {
                    $this->request->setParam('q',$keyword);
                    $productCollection =  $this->layerResolver->get()->getProductCollection();
                    if ($productCollection->count()) {
                        return $this->redirectSearchPage($keyword);
                    }
                }
            }
        }
        return $result;
    }

    private function redirectSearchPage($keyword) {
        $redirectResult = $this->redirectResult->create();
        $redirectResult->setUrl('/catalogsearch/result/?q='.urlencode($keyword));
        return $redirectResult;
    }
}