<?php


namespace Magenest\DeliveryDate\Model\ResourceModel\TimeInterval;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'time_interval_id';

    public function _construct()
    {
        $this->_init(\Magenest\DeliveryDate\Model\TimeInterval::class,\Magenest\DeliveryDate\Model\ResourceModel\TimeInterval::class);
    }
}