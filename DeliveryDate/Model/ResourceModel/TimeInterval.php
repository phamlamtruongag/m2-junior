<?php


namespace Magenest\DeliveryDate\Model\ResourceModel;


class TimeInterval extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init('magenest_delivery_time_interval','time_interval_id');
    }
}