<?php


namespace Magenest\DeliveryDate\Model;


class DeliveryDateConfigProvider implements \Magento\Checkout\Model\ConfigProviderInterface
{
    protected $deliveryDateConfig;

    public function __construct(\Magenest\DeliveryDate\Helper\DeliveryDateConfig $deliveryDateConfig)
    {
        $this->deliveryDateConfig = $deliveryDateConfig;
    }

    public function getConfig()
    {
        $configData = [];
        $configData['deliveryDateConfig']['sameDayDeliveryCollection'] = $this->deliveryDateConfig->getSameDayDeliveryCollection();
        $configData['deliveryDateConfig']['sameDayDeliveryStatus'] = $this->deliveryDateConfig->getSameDayDeliveryStatus();
        $configData['deliveryDateConfig']['dateFormat'] = $this->deliveryDateConfig->getDateFormat();
        $configData['deliveryDateConfig']['offDay'] = $this->deliveryDateConfig->getOffDay();
        $configData['deliveryDateConfig']['leadTime'] = $this->deliveryDateConfig->getLeadTime();
        $configData['deliveryDateConfig']['maximalInterval'] = $this->deliveryDateConfig->getMaximalInterval();
        $configData['deliveryDateConfig']['isEnable'] = $this->deliveryDateConfig->getModuleStatus();
        $configData['deliveryDateConfig']['noticeContent'] = $this->deliveryDateConfig->getNoticeContent();
        $configData['deliveryDateConfig']['commentStatus'] = $this->deliveryDateConfig->getCommentStatus();
        $configData['deliveryDateConfig']['intervalTime'] = $this->deliveryDateConfig->getAllIntervalTime();
        return $configData;
    }
}