<?php


namespace Magenest\DeliveryDate\Model\Source\TimeInterval;


class TimeIntervalStatus extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    public static function getOptionArray()
    {
        return [
            \Magenest\DeliveryDate\Model\TimeInterval::ENABLE => __('Enable'),
            \Magenest\DeliveryDate\Model\TimeInterval::DISABLE => __('Disable')
        ];
    }

    public function getAllOptions()
    {
        $result = [];
        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }
        return $result;
    }
}