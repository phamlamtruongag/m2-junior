<?php


namespace Magenest\DeliveryDate\Model\Source\TimeInterval;


class DateFormat extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{


    public static function getOptionArray()
    {
        return [
            'dd/mm/yy' => __('dd/mm/yy'),
            'mm/dd/yy' => __('mm/dd/yy'),
        ];
    }

    public function getAllOptions()
    {
        $result = [];
        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }
        return $result;
    }
}