<?php


namespace Magenest\DeliveryDate\Model;


class TimeInterval extends \Magento\Framework\Model\AbstractModel
{
    const ENABLE  = 1;
    const DISABLE = 0;
    const ORDER_VIEW_PAGE = 1;
    const ACTION_ORDER_PAGE = 2;
    const INVOICE_VIEW_PAGE = 3;
    const SHIPMENT_VIEW_PAGE = 4;
    const ORDER_INFO_PAGE = 5;

    public function _construct()
    {
        $this->_init(\Magenest\DeliveryDate\Model\ResourceModel\TimeInterval::class);
    }
}