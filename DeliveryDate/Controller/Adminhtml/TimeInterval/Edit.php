<?php


namespace Magenest\DeliveryDate\Controller\Adminhtml\TimeInterval;


class Edit extends \Magento\Backend\App\Action
{
    protected $pageResultFactory;

    public function __construct(
        \Magento\Framework\View\Result\PageFactory $pageResultFactory,
        \Magento\Backend\App\Action\Context $context)
    {
        parent::__construct($context);
        $this->pageResultFactory = $pageResultFactory;
    }

    public function execute()
    {
        $resultPage = $this->pageResultFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('Delivery Time Form'));
        return $resultPage;
    }
}