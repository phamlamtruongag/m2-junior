<?php


namespace Magenest\DeliveryDate\Controller\Adminhtml\TimeInterval;


class Delete extends \Magento\Backend\App\Action
{
    protected $timeIntervalModel;
    protected $timeIntervalResource;
    protected $logger;
    protected $cache;

    public function __construct(
        \Magento\Framework\App\Cache\TypeListInterface $cache,
        \Psr\Log\LoggerInterface $logger,
        \Magenest\DeliveryDate\Model\TimeIntervalFactory $timeIntervalModel,
        \Magenest\DeliveryDate\Model\ResourceModel\TimeInterval $timeIntervalResource,
        \Magento\Backend\App\Action\Context $context)
    {
        parent::__construct($context);
        $this->timeIntervalModel = $timeIntervalModel;
        $this->timeIntervalResource = $timeIntervalResource;
        $this->logger = $logger;
        $this->cache = $cache;
    }

    public function execute()
    {
        try{
            /** @var \Magenest\DeliveryDate\Model\TimeInterval $timeInterval */
            $timeInterval = $this->timeIntervalModel->create();
            if ($this->_request->getParam('id') ?? false){
                $this->timeIntervalResource->load($timeInterval,$this->_request->getParam('id'));
                $this->timeIntervalResource->delete($timeInterval);
            }
            $this->cache->invalidate('full_page');
            $this->messageManager->addSuccess(__('This record has been deleted.'));
        }catch (\Exception $exception){
            $this->messageManager->addError($exception->getMessage());
            $this->logger->critical($exception->getMessage());
        }
        $this->_redirect('*/*/index');
    }
}