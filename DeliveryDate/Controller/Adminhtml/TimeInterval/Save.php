<?php


namespace Magenest\DeliveryDate\Controller\Adminhtml\TimeInterval;


class Save extends \Magento\Backend\App\Action
{
    protected $timeIntervalModel;
    protected $timeIntervalResource;
    protected $logger;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magenest\DeliveryDate\Model\TimeIntervalFactory $timeIntervalModel,
        \Magenest\DeliveryDate\Model\ResourceModel\TimeInterval $timeIntervalResource,
        \Magento\Backend\App\Action\Context $context)
    {
        parent::__construct($context);
        $this->timeIntervalModel = $timeIntervalModel;
        $this->timeIntervalResource = $timeIntervalResource;
        $this->logger = $logger;
    }

    public function execute()
    {
        $params = $this->_request->getParams();
        try {
            /** @var \Magenest\DeliveryDate\Model\TimeInterval $timeInterval */
            $timeInterval = $this->timeIntervalModel->create();
            if ($params['time_interval_id'] ?? false) {
                $this->timeIntervalResource->load($timeInterval,$params['time_interval_id']);
            }
            $timeInterval->setTimeIntervalStatus($params['time_interval_status'] ?? 0);
            $timeInterval->setVisibleStores(json_encode($params['visible_stores'] ?? []));
            $timeInterval->setCustomerGroupIds(json_encode($params['customer_group_ids'] ?? []));
            $timeInterval->setTimeIntervalData(json_encode($params['time_interval_data'] ?? []));
            $this->timeIntervalResource->save($timeInterval);
        } catch (\Exception $exception) {
            $this->logger->critical($exception->getMessage());
            $this->messageManager->addError($exception->getMessage());
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        $redirectBack = $this->getRequest()->getParam('back', false);
        if ($redirectBack === 'edit') {
            $resultRedirect->setPath(
                '*/*/edit',
                ['id' => $timeInterval->getTimeIntervalId(), 'back' => null, '_current' => true]
            );
        } else {
            $resultRedirect->setPath('*/*/index');
        }
        return $resultRedirect;
    }
}