<?php


namespace Magenest\DeliveryDate\Controller\Adminhtml\TimeInterval;


class NewAction extends \Magento\Backend\App\Action
{
    public function execute()
    {
        $this->_forward('edit');
    }
}