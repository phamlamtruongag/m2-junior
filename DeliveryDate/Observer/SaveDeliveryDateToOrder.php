<?php


namespace Magenest\DeliveryDate\Observer;


class SaveDeliveryDateToOrder implements \Magento\Framework\Event\ObserverInterface
{
    protected $objectCopyService;

    public function __construct(
        \Magento\Framework\DataObject\Copy $objectCopyService
    ) {
        $this->objectCopyService = $objectCopyService;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getEvent()->getData('order');
        $quote = $observer->getEvent()->getData('quote');
        $this->objectCopyService->copyFieldsetToTarget('delivery_date', 'to_order', $quote, $order);
        return $this;
    }
}