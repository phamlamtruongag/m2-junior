<?php


namespace Magenest\DeliveryDate\Cron;


class SendMailNotifyShipment
{
    const READY_TO_SEND_NOTIFY_SHIPMENT = 1;
    const SENT_NOTIFY_SHIPMENT = 2;

    protected $emailDeliveryHelper;
    protected $json;
    protected $logger;
    protected $orderRepository;
    protected $orderCollection;
    protected $timezone;
    protected $dateTime;

    public function __construct(
        \Magenest\DeliveryDate\Helper\Email $emailDeliveryHelper,
        \Magento\Framework\Serialize\Serializer\Json $json,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollection,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
    ) {
        $this->emailDeliveryHelper = $emailDeliveryHelper;
        $this->json = $json;
        $this->logger = $logger;
        $this->orderRepository = $orderRepository;
        $this->orderCollection = $orderCollection;
        $this->timezone = $timezone;
        $this->dateTime = $dateTime;
    }

    public function execute()
    {
        $orderCollection = $this->orderCollection->create()->addFieldToFilter('notify_shipment',self::READY_TO_SEND_NOTIFY_SHIPMENT);
        /** @var \Magento\Sales\Model\Order $order */
        foreach ($orderCollection as $order) {
            if ($this->checkTimeToSendMail($order))
            $this->emailDeliveryHelper->sendEmail($order->getCustomerEmail());
            $order->setNotifyShipment(self::SENT_NOTIFY_SHIPMENT);
        }
        return $this;

    }

    private function checkTimeToSendMail(\Magento\Sales\Model\Order $order)
    {
        $currentTime = $this->dateTime->gmtDate();
        $shippingTime = $order->getDeliveryDatetime();
        if ($shippingTime) {
            $datetime1 = new \DateTime($currentTime);
            $datetime2 = new \DateTime($shippingTime);
            $interval = $datetime1->diff($datetime2);
            if (!$interval->y && !$interval->m && !$interval->d && !$interval->h) {
                return true;
            }
        }
        return false;
    }

}