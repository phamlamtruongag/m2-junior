define([
    'Magento_Ui/js/form/element/date'
], function(Date) {
    'use strict';
    return Date.extend({
        defaults: {
            options: {
                timeFormat: 'HH:mm',
                // dateFormat: 'HH:mm',
                showsTime: true,
                timeOnly: true,
                currentText: 'Now'
            },
        }
    });
});