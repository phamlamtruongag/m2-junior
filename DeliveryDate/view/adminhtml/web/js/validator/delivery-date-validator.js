require([
    'jquery',
    'uiRegistry',
    'Magento_Ui/js/lib/validation/validator',
    'jquery/validate',
    'mage/translate',
    'domReady!'
], function ($, registry, validator) {
    validator.addRule(
        'validate-from-to',
        function (value) {
            var rowId = 0,
                nameInputFrom,
                nameInputTo,
                dateFrom,
                dateTo;
            do {
                nameInputFrom = 'deliverytime_data[deliverytime_data]['+rowId+']'+'[from]';
                nameInputTo = 'deliverytime_data[deliverytime_data]['+rowId+']'+'[to]';
                dateFrom =  $("input[name='"+nameInputFrom+"']").val();
                dateTo = $("input[name='"+nameInputTo+"']").val();
                if (dateFrom && dateTo && dateTo < dateFrom){
                    return false;
                }
                rowId++;
            } while (dateFrom && dateTo);
            return true
        },
        $.mage.__('To time greater than from time')
    )
});
