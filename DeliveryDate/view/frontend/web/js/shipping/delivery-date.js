define([
    'jquery',
    'ko',
    'uiComponent',
    'Magento_Checkout/js/model/quote'
], function ($, ko, Component, quote) {
    'use strict';
    var today = new Date();
    var minDate = new Date();
    var maxDate = new Date();
    var isNewDay = 0;

    if (window.checkoutConfig.deliveryDateConfig.sameDayDeliveryStatus) {
        var time = window.checkoutConfig.deliveryDateConfig.sameDayDeliveryCollection.split(',');
        var totalSeconds = parseInt(time[0])*3600 + parseInt(time[1])*60 + parseInt(time[2]);
        var todayTime = today.getHours()*3600 + today.getMinutes()*60 +  today.getSeconds();
        if (todayTime > totalSeconds) {
            isNewDay = 1;
        }
    }

    minDate.setDate(today.getDate() + parseInt(window.checkoutConfig.deliveryDateConfig.leadTime) + parseInt(isNewDay));
    maxDate.setDate(today.getDate() + parseInt(window.checkoutConfig.deliveryDateConfig.maximalInterval) + parseInt(isNewDay));

    return Component.extend({
        defaults: {
            template: 'Magenest_DeliveryDate/shipping/delivery-date',
        },
        availableInterval: window.checkoutConfig.deliveryDateConfig.intervalTime,
        canVisibleBlock: window.checkoutConfig.deliveryDateConfig.isEnable,
        deliveryDate: ko.observable(null),
        deliveryTimeInterval: ko.observable(null),
        dateOptions: {
            dateFormat:  window.checkoutConfig.deliveryDateConfig.dateFormat,
            showButtonPanel: true,
            minDate: minDate,
            maxDate: maxDate,
            beforeShowDay: function (date) {
                var day=date.getDay();
                var offDay = window.checkoutConfig.deliveryDateConfig.offDay
                if(offDay.includes(day))
                {
                    return [false];
                }
                return [true];
            }
        },
        noteByAdmin: window.checkoutConfig.deliveryDateConfig.noticeContent,
        enableComment: window.checkoutConfig.deliveryDateConfig.commentStatus,
        initialize: function () {
            var self = this;
            this._super();
            this.deliveryDatetime = ko.computed(function () {
                if (self.deliveryDate() && self.deliveryTimeInterval()) {
                    var selectedDate = self.deliveryDate();
                    var dateKey = window.checkoutConfig.deliveryDateConfig.dateFormat.split('/');
                    var dateValue = selectedDate.split('/');
                    var dateObject = dateValue.reduce(function (result, field, index) {
                        result[dateKey[index]] = field;
                        return result;
                    }, {})
                    var selectedTime = self.deliveryTimeInterval();
                    var time = selectedTime.split(' ')[0].split(':');
                    var dateTime = new Date();
                    dateTime.setDate(dateObject['dd']);
                    dateTime.setMonth(dateObject['mm']);
                    dateTime.setFullYear(dateObject['yy']);
                    dateTime.setHours(time[0]);
                    dateTime.setMinutes(time[1]);
                    var dateTimeValue = dateTime.getFullYear()+'-'+dateTime.getMonth()+'-'+dateTime.getDate()+' '+dateTime.getHours()+':'+dateTime.getMinutes()+':'+dateTime.getSeconds();
                    return dateTimeValue;
                }
               return '';
            });
            return this;
        },
    });
});