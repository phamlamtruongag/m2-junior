define([
    'jquery',
    'ko',
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'Magento_Customer/js/customer-data',
], function ($, ko, Component, quote, storage) {
    'use strict';
    return Component.extend({
        defaults: {
            template: 'Magenest_DeliveryDate/shipping/delivery-date-information',
        },
        canVisibleBlock: window.checkoutConfig.deliveryDateConfig.isEnable,
        deliveryDate: ko.observable(null),
        deliveryTime: ko.observable(null),
        deliveryComment: ko.observable(null),

        initialize: function () {
            var self = this;

            this._super();

            quote.shippingAddress.subscribe(function (address) {
                if (typeof address['extension_attributes'] !== 'undefined') {
                    if (typeof address['extension_attributes']['delivery_date'] !== 'undefined') {
                        self.deliveryDate(address['extension_attributes']['delivery_date'])
                    }
                    if (typeof address['extension_attributes']['delivery_time_interval'] !== 'undefined') {
                        self.deliveryTime(address['extension_attributes']['delivery_time_interval'])
                    }
                    if (typeof address['extension_attributes']['delivery_comment'] !== 'undefined') {
                        self.deliveryComment(address['extension_attributes']['delivery_comment'])
                    }
                }
            });

            return this;
        },
    });
});