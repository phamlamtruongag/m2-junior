define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote'
], function ($, wrapper, quote) {
    'use strict';

    return function (setShippingInformationAction) {
        return wrapper.wrap(setShippingInformationAction, function (originalAction) {
            var shippingAddress = quote.shippingAddress();
            if (shippingAddress['extension_attributes'] === undefined) {
                shippingAddress['extension_attributes'] = {};
            }
            shippingAddress['extension_attributes']['delivery_date'] = jQuery('[name="delivery-date"]').val();
            shippingAddress['extension_attributes']['delivery_time_interval'] = jQuery('[name="delivery-time-interval"]').val();
            shippingAddress['extension_attributes']['delivery_comment'] = jQuery('[name="delivery-comment"]').val();
            shippingAddress['extension_attributes']['delivery_datetime'] = jQuery('[name="delivery-datetime"]').val();
            quote.shippingAddress(shippingAddress);
            return originalAction();
        });
    };
});