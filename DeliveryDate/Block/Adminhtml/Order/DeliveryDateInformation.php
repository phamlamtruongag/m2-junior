<?php


namespace Magenest\DeliveryDate\Block\Adminhtml\Order;


use Magento\Backend\Block\Template;

class DeliveryDateInformation extends Template
{
    protected $orderRepository;

    public function __construct(
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        Template\Context $context,
        array $data = [],
        ?\Magento\Framework\Json\Helper\Data $jsonHelper = null,
        ?\Magento\Directory\Helper\Data $directoryHelper = null
    ) {
        parent::__construct($context, $data, $jsonHelper, $directoryHelper);
        $this->orderRepository = $orderRepository;
    }

    public function getOrder()
    {
        $id = $this->getRequest()->getParam('order_id');
        return $id ? $this->orderRepository->get($id) : null;
    }
}