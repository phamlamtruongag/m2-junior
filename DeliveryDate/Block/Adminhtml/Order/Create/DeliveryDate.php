<?php


namespace Magenest\DeliveryDate\Block\Adminhtml\Order\Create;


use Magento\Backend\Block\Template;

class DeliveryDate extends Template
{
    protected $deliveryDateConfig;
    protected $quoteSession;

    public function __construct(
        \Magento\Backend\Model\Session\Quote $quoteSession,
        \Magenest\DeliveryDate\Helper\DeliveryDateConfig $deliveryDateConfig,
        Template\Context $context,
        array $data = [],
        ?\Magento\Framework\Json\Helper\Data $jsonHelper = null,
        ?\Magento\Directory\Helper\Data $directoryHelper = null
    ) {
        parent::__construct($context, $data, $jsonHelper, $directoryHelper);
        $this->deliveryDateConfig = $deliveryDateConfig;
        $this->quoteSession = $quoteSession;
    }

    public function getAllInterval()
    {
        return $this->deliveryDateConfig->getAllIntervalTime();
    }

    public function getDeliveryDate()
    {
        return $this->quoteSession->getQuote()->getDeliveryDate() ?? '';
    }

    public function getDeliveryTimeInterval()
    {
        return $this->quoteSession->getQuote()->getDeliveryTimeInterval() ?? '';
    }

    public function getDeliveryComment()
    {
        return $this->quoteSession->getQuote()->getDeliveryComment() ?? '';
    }
}