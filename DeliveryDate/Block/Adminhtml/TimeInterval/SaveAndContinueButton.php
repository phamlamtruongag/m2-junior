<?php


namespace Magenest\DeliveryDate\Block\Adminhtml\TimeInterval;


class SaveAndContinueButton implements \Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface
{
    public function getButtonData()
    {
        return [
            'label' => __('Save and Continue Edit'),
            'class' => 'save',
            'on_click' => '',
        ];
    }
}