<?php


namespace Magenest\DeliveryDate\Plugin\Quote;


class SaveDataToQuote
{
    protected $quoteRepository;
    protected $dateTimeFactory;
    protected $timezone;

    public function __construct(
        \Magento\Quote\Model\QuoteRepository $quoteRepository,
        \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateTimeFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
    ) {
        $this->dateTimeFactory = $dateTimeFactory;
        $this->quoteRepository = $quoteRepository;
        $this->timezone = $timezone;
    }
    
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        $extAttributes = $addressInformation->getShippingAddress()->getExtensionAttributes();
        $deliveryDatetime = $extAttributes->getDeliveryDatetime();
        $deliveryDate = $extAttributes->getDeliveryDate();
        $deliveryTimeInterval = $extAttributes->getDeliveryTimeInterval();
        $deliveryComment = $extAttributes->getDeliveryComment();
        $quote = $this->quoteRepository->getActive($cartId);
        $quote->setDeliveryDate($deliveryDate);
        $quote->setDeliveryTimeInterval($deliveryTimeInterval);
        $quote->setDeliveryComment($deliveryComment);
        $quote->setDeliveryDatetime($deliveryDatetime);
    }
}