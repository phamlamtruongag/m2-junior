<?php


namespace Magenest\DeliveryDate\Helper;


class DeliveryDateConfig extends \Magento\Framework\App\Helper\AbstractHelper
{
    const MODULE_STATUS = 'magenest_deliverydate/general/enabled';
    const LEAD_TIME = 'magenest_deliverydate/general/lead_time';
    const MAXIMAL_INTERVAL = 'magenest_deliverydate/general/maximal_delivery_interval';
    const NOTICE_BY_ADMIN = 'magenest_deliverydate/general/notice_by_admin';
    const SAME_DAY_STATUS = 'magenest_deliverydate/general/same_day_delivery_after';
    const COMMENT_STATUS = 'magenest_deliverydate/general/enable_comment';
    const OFF_DAY = 'magenest_deliverydate/general/day_disable';
    const DATE_FORMAT = 'magenest_deliverydate/general/date_format';
    const SAME_DAY_DELIVERY_STATUS = 'magenest_deliverydate/general/same_day_delivery_after';
    const SAME_DAY_DELIVERY_COLLECTION = 'magenest_deliverydate/general/collection_time_delivery_after';

    protected $storeManager;
    protected $customerSession;
    protected $timeIntervalModel;
    protected $timeIntervalResource;
    protected $timeIntervalCollection;
    protected $json;
    protected $timezone;

    public function __construct(
        \Magenest\DeliveryDate\Model\TimeIntervalFactory $timeIntervalModel,
        \Magenest\DeliveryDate\Model\ResourceModel\TimeInterval $timeIntervalResource,
        \Magenest\DeliveryDate\Model\ResourceModel\TimeInterval\CollectionFactory $timeIntervalCollection,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Serialize\Serializer\Json $json,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Helper\Context $context)
    {
        parent::__construct($context);
        $this->timeIntervalModel = $timeIntervalModel;
        $this->timeIntervalResource = $timeIntervalResource;
        $this->timeIntervalCollection = $timeIntervalCollection;
        $this->storeManager = $storeManager;
        $this->customerSession = $customerSession;
        $this->json = $json;
        $this->timezone = $timezone;
    }

    public function getModuleStatus() {
        return $this->scopeConfig->isSetFlag(self::MODULE_STATUS);
    }

    public function getSameDayDeliveryStatus(){
        return $this->scopeConfig->isSetFlag(self::SAME_DAY_DELIVERY_STATUS);
    }

    public function getSameDayDeliveryCollection(){
        return $this->scopeConfig->getValue(self::SAME_DAY_DELIVERY_COLLECTION);
    }

    public function getOffDay() {
        return $this->scopeConfig->getValue(self::OFF_DAY);
    }

    public function getDateFormat(){
        return $this->scopeConfig->getValue(self::DATE_FORMAT);
    }

    public function getLeadTime() {
        return $this->scopeConfig->getValue(self::LEAD_TIME);
    }

    public function getMaximalInterval() {
        return $this->scopeConfig->getValue(self::MAXIMAL_INTERVAL);
    }

    public function getNoticeContent() {
        return $this->scopeConfig->getValue(self::NOTICE_BY_ADMIN);
    }

    public function getSameDayStatus() {
        return $this->scopeConfig->getValue(self::SAME_DAY_STATUS);
    }

    public function getCommentStatus() {
        return $this->scopeConfig->isSetFlag(self::COMMENT_STATUS);
    }

    public function getAllIntervalTime() {
        $timeIntervalCollection = $this->timeIntervalCollection->create()->addFieldToFilter('time_interval_status',\Magenest\DeliveryDate\Model\TimeInterval::ENABLE);
        $timeIntervalDataResult = [];
        /** @var \Magenest\DeliveryDate\Model\TimeInterval $timeInterval */
        foreach ($timeIntervalCollection as $timeInterval) {
            if ($this->checkCustomerGroup($timeInterval) && $this->checkVisibleStores($timeInterval)) {
                $timeIntervalData = $this->json->unserialize($timeInterval->getTimeIntervalData() ?? 'null')['time_interval_data'] ?? [];
                foreach ($timeIntervalData as $item) {
                    $newTimeInterval =  date_create($item['from'])->format('H:i').' - '.date_create($item['to'])->format('H:i');
                    if (!in_array($newTimeInterval,$timeIntervalData)) {
                        $timeIntervalDataResult[] = $newTimeInterval;
                    }
                }
            }
        }
        return $timeIntervalDataResult;
    }

    private function checkCustomerGroup(\Magenest\DeliveryDate\Model\TimeInterval $timeInterval) {
        $customerGroupId = $this->customerSession->getCustomerGroupId();
        $customerGroupIds = $this->json->unserialize($timeInterval->getCustomerGroupIds() ?? 'null');
        return in_array($customerGroupId,$customerGroupIds) || in_array(\Magento\Customer\Api\Data\GroupInterface::CUST_GROUP_ALL,$customerGroupIds);
    }

    private function checkVisibleStores(\Magenest\DeliveryDate\Model\TimeInterval $timeInterval) {
        $storeId = $this->storeManager->getStore()->getId();
        $visibleStores = $this->json->unserialize($timeInterval->getVisibleStores() ?? 'null');
        return in_array($storeId,$visibleStores) || in_array(\Magento\Cms\Ui\Component\Listing\Column\Cms\Options::ALL_STORE_VIEWS,$visibleStores);
    }
}