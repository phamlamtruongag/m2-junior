<?php


namespace Magenest\DeliveryDate\Ui\Component\DataProvider;


class TimeInterval extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    protected $_loadedData;
    protected $_collectionFactory;
    protected $_json;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Magenest\DeliveryDate\Model\ResourceModel\TimeInterval\CollectionFactory $collectionFactory,
        \Magento\Framework\Serialize\Serializer\Json $json,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->_collectionFactory=$collectionFactory;
        $this->collection=$this->_collectionFactory->create();
        $this->_json = $json;
    }

    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $item) {
            $data = $item->getData();
            $data['visible_stores'] = $this->_json->unserialize($data['visible_stores'] ?? null) ?? [];
            $data['customer_group_ids'] = $this->_json->unserialize($data['customer_group_ids'] ?? null) ?? [];
            $data['time_interval_data'] = $this->_json->unserialize($data['time_interval_data'] ?? 'null') ?? [];
            $this->_loadedData[$item->getId()] = $data;
        }
        return $this->_loadedData;
    }
}