<?php


namespace Magenest\DeliveryDate\Ui\Component\Listing\Column\TimeInterval;


class ViewAction extends \Magento\Ui\Component\Listing\Columns\Column
{
    protected $_urlBuilder;
    protected $frontendUrl;

    public function __construct(
        \Magento\Framework\UrlInterface $urlBuider,
        \Magento\Framework\Url $frontendUrl,
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ){
        $this->_urlBuilder = $urlBuider;
        $this->frontendUrl = $frontendUrl;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if(isset($dataSource['data']['items'])){
            foreach ($dataSource['data']['items'] as &$item) {
                $item[$this->getData('name')]['edit'] = [
                    'href' => $this->_urlBuilder->getUrl(
                        'magenest_deliverydate/timeInterval/edit',
                        ['id' => $item['time_interval_id']]
                    ),
                    'label' => __('Edit'),
                    'hidden' => false,
                ];
                $item[$this->getData('name')]['delete'] = [
                    'href' => $this->_urlBuilder->getUrl(
                        'magenest_deliverydate/timeInterval/delete',
                        ['id' => $item['time_interval_id']]
                    ),
                    'label' => __('Delete'),
                    'confirm' => [
                        'title' => __('Delete this record', ),
                        'message' => __('Are you sure you want to delete this record?')
                    ],
                    'hidden' => false,
                ];
            }
        }
        return $dataSource;
    }
}