<?php


namespace Magenest\CollectShipping\Plugin\Quote;


class SaveAccountNumber
{
    protected $quoteRepository;

    public function __construct(
        \Magento\Quote\Model\QuoteRepository $quoteRepository
    ) {
        $this->quoteRepository = $quoteRepository;
    }

    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        $extAttributes = $addressInformation->getShippingAddress()->getExtensionAttributes();
        $collectShipping = $extAttributes->getCollectShipping();
        $accountNumber = $extAttributes->getAccountNumber();
        $quote = $this->quoteRepository->getActive($cartId);
        $quote->setCollectShipping($collectShipping);
        $quote->setAccountNumber($accountNumber);
    }
}