<?php


namespace Magenest\CollectShipping\Ui\Component\Listing\Column;


class ShippingInformation extends \Magento\Ui\Component\Listing\Columns\Column
{
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $item['shipping_information'] .= '<div>'. $item['account_number'].'</div>';
            }
        }
        return $dataSource;
    }
}