<?php


namespace Magenest\CollectShipping\Observer;


class SaveAccountNumberToOrder implements \Magento\Framework\Event\ObserverInterface
{
    protected $objectCopyService;

    public function __construct(
        \Magento\Framework\DataObject\Copy $objectCopyService
    ) {
        $this->objectCopyService = $objectCopyService;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getEvent()->getData('order');
        $quote = $observer->getEvent()->getData('quote');
        $this->objectCopyService->copyFieldsetToTarget('account_number', 'to_order', $quote, $order);
        return $this;
    }
}