<?php


namespace Magenest\ExtendProductListing\Ui\Component\Listing\Column;

use Magento\Ui\Component\Listing\Columns\Column;

class FrontendProductLink extends Column
{
    protected $productResource;
    protected $product;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product $productResource,
        \Magento\Catalog\Model\ProductFactory $product,
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->productResource = $productResource;
        $this->product = $product;
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $product = $this->product->create();
                $this->productResource->load($product,$item['entity_id']);
                $item[$this->getData('name')]['view'] = [
                    'href' => $product->getProductUrl(),
                    'label' => __('View'),
                    'hidden' => false,
                    'target' => '_blank'
                ];
            }
        }
        return $dataSource;
    }
}